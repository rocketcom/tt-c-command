import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';

import '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/polymer/lib/elements/dom-if.js';
import '@polymer/polymer/lib/elements/array-selector.js';
import '@polymer/polymer/lib/utils/path.js';

/* eslint-disable no-unused-vars */
import {RuxStatus} from '@astrouxds/rux-status/rux-status.js';
import {RuxIcon} from '@astrouxds/rux-icon/rux-icon.js';
import {TtcSystemHealthItem} from './system-health-item.js';

/* eslint-enable no-unused-vars */

import template from './system-health.html';
import css from './system-health.css';

/**
 * @polymer
 * @extends HTMLElement
 */
export class TtcSystemHealth extends PolymerElement {
  static get properties() {
    return {
      lock: {
        type: Object,
      },
      telemetry: {
        type: Object,
      },
      vcc: {
        type: Object,
      },
    };
  }

  static get template() {
    return html([
      `
        <style include="astro-css">
          ${css}
        </style> 
        ${template}`,
    ]);
  }

  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
  }

  disconnectedCallback() {
    super.disconnectedCallback();
  }

  ready() {
    super.ready();
  }
}

customElements.define('ttc-system-health', TtcSystemHealth);
