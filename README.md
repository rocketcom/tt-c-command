# TT&C Sample Apps: Command

## Description
The TT&C Command app is designed to be used for sending and receiving communications with a satellite during a contact. It contains status and command data for only a single satellite - one currently in a pass. Operators could open multiple command windows if they are managing several passes simultaneously.

UX research underscored the fact that commanding is the core of TT&C operations and that existing systems often split the functionality to support it across application windows. This makes finding critical information and actions more difficult and time consuming. Therefore, a key tenet of the Command app design was to centralize the core functionality into a single window.

Another aspect of the design aimed at reducing cognitive load is to give operators the ability to automate execution of the Pass Plan. Here, a plan can be run in a Manual mode, in which an operator determines when the next step is executed, Automated mode, in which the steps are executed sequentially by the system itself, or Semi-Auto mode, a hybrid of the two.  Plans are set up to run in a default mode, and operators with sufficient privileges could override this as the situation dictates. Operators also could have the option to add commands to the Pass Plan queue if necessary.

## Prerequisites
1. A UNIX-like OS (Linux, Mac OS, FreeBSD, etc.)*
2. [Node.js](https://nodejs.org/)
3. [npm](https://www.npmjs.com/get-npm)
4. [Polymer](https://www.polymer-project.org/)
5. [EGS Web Socket Server](https://bitbucket.org/rocketcom/egs-socket-server/src/master/) (optional)**
6. [EGS Web Services Server](https://bitbucket.org/rocketcom/egs-data-services/src/master/) (optional)**

\* It may be possible to run on Windows but has not been thoroughly tested and is not recommended.
\*\* You only need to run your own Web Socket and Web Services servers if you are behind a firewall that prevents you from accessing the ones we provide at <wss://sockets.astrouxds.com> and <https://services.astrouxds.com> respectively.


## Getting Started
### Clone this repository

`git clone git@bitbucket.org:rocketcom/tt-c-command.git`

### Install the Polymer CLI

`npm i -g polymer-cli`

### Install NPM modules for this project

`npm i`

### Create a symbolic link to the appropriate file in the `config` directory.

We recommend linking to config.local.json:

`ln -s config.local.json config.json`

However, if you are running your own Web Socket and Web Services servers (see above) you will need to create your own config file that points to your own servers. Use one of the existing config files as a template and substitute the appropriate URLs.

### For local development and testing

`npm run start`

### To build for production

`npm run build`

### Load the application(s) in your browser

Assuming you've linked your config file to config.local.json:

<http://localhost:9001>

Otherwise, see values from your config file and/or the output from `npm run start`